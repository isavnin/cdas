# CDAS REST via MATLAB #

This project provides an interface to access to CDAS ([CDAWeb](http://cdaweb.gsfc.nasa.gov/)) [REST](http://en.wikipedia.org/wiki/Representational_state_transfer) web services from MATLAB. 

The documentation to CDAS REST web services can be found at http://cdaweb.sci.gsfc.nasa.gov/WebServices/REST/

Using this library it is possible to fetch lists of dataviews, observatories, datasets and variables from CDAWeb. It is also possible to get data from any of the observatories presented on CDAWeb on the fly.

# Documentation #

All the functionality is encapsulated in the class CDAS. First, one should initialize an instance of the CDAS class:

```
#!matlab

cdaweb = CDAS;
```

### Dataviews ###

The missions and hence data are grouped to dataviews in CDAWeb. You can get the list of dataviews by the following command:

```
#!matlab

dataviews = cdaweb.getDataviews();
```
Typically, you would want to always use the dataview istp_public, which corresponds to the missions after year 1992.

### Observatory groups ###

The observatory groups can be fetched in the following way:


```
#!matlab

observatoryGroups = cdaweb.getObservatoryGroups('istp_public');
```
A typical example of an observatory group is THEMIS, which consists of several spacecraft/observatories.

### Observatories ###

The list of observatories can be fetched in the following way:


```
#!matlab

observatories = cdaweb.getObservatories('istp_public');
```
For example, in case of STEREO observatory group the related observatories would be Ahead and Behind.

### Datasets ###

Dataset is a combination of variables grouped into a collection. Usually, a dataset corresponds to a separate experiment of the mission, calibration level or some other purpose.

For example, you can get the list of available datasets of the GOES_12 observatory (which belongs to the observatory group GOES) from 23 to 24 September 2005 with the following command:

```
#!matlab

datasets = cdaweb.getDatasets('istp_public', 'GOES', 'GOES_12', '2005-09-23 00:00:00', '2005-09-24 00:00:00');
```

Observatory group, observatory and start and end times are optional parameters. So, for instance, you can get the all the datasets for the STEREO observatory group for both Ahead and Behind spacecraft and for all the period of the mission like that:


```
#!matlab

datasets = cdaweb.getDatasets('istp_public', 'STEREO', NaN, NaN, NaN);
```

### Variables ###

Each dataset contains one or more variables. The list of the variables of a given dataset can be obtained with the following command (in this example for one of the GOES datasets):


```
#!matlab

variables = cdaweb.getVariables('istp_public', 'G0_K0_MAG');
```

### Data ###

Finally, after you found out what particular dataset and variables you are interested in you can fetch the data:


```
#!matlab

data = cdaweb.getData('istp_public', 'G0_K0_MAG', '2005-09-23 00:00:00', '2005-09-24 00:00:00', 'B_GSE_c');
```
The data variable will contain the columns of dates and data you have requested. In this particular case, it will contain magnetic field vector in GSE coordinates.

You can ask for any number of variables at once. For example, this command will return magnetic field vectors in both GSE and GSM coordinates:


```
#!matlab

data = cdaweb.getData('istp_public', 'G0_K0_MAG', '2005-09-23 00:00:00', '2005-09-24 00:00:00', 'B_GSE_c', 'B_GSM_c');
```