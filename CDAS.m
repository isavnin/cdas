
% Alexey Isavnin, 2015

% This class provides access to CDAWeb data

classdef CDAS
    
    properties(Constant)
        % main address of REST services of CDAWeb
        url = 'http://cdaweb.gsfc.nasa.gov/WS/cdasr/1/';
        % temporary file for xml responses from CDAWeb REST
        xmlFile = './.cdas_xml.xml';
        % temporary file for ascii data
        dataFile = './.cdas_data.txt';
    end
    
    methods
        % get dataviews as a parsed xml structure
        function dataviews = getDataviewsXml(obj)
            request = [obj.url,'dataviews'];
            dataviews = obj.process(request).Dataviews;
        end
        
        % get dataviews as an array
        function dataviews = getDataviews(obj)
            xml = obj.getDataviewsXml();
            n = length(xml.DataviewDescription);
            dataviews = cell(1, n);
            for i = 1:n
                dataviews{i} = xml.DataviewDescription{i}.Id.Text;
            end
        end
        
        % get observatory groups as a parsed xml structure
        function observatoryGroups = getObservatoryGroupsXml(obj, dataview)
            request = [obj.url,'dataviews/',dataview,'/observatoryGroups'];
            observatoryGroups = obj.process(request).ObservatoryGroups;
        end
        
        % get observatory groups as an array
        function observatoryGroups = getObservatoryGroups(obj, dataview)
            xml = obj.getObservatoryGroupsXml(dataview);
            n = length(xml.ObservatoryGroupDescription);
            observatoryGroups = cell(1, n);
            for i = 1:n
                observatoryGroups{i} = xml.ObservatoryGroupDescription{i}.Name.Text;
            end
        end
        
        % get observatories as a parsed xml structure
        function observatories = getObservatoriesXml(obj, dataview)
            request = [obj.url,'dataviews/',dataview,'/observatories'];
            observatories = obj.process(request).Observatories;
        end
        
        % get observatories as an array
        function observatories = getObservatories(obj, dataview)
            xml = obj.getObservatoriesXml(dataview);
            n = length(xml.ObservatoryDescription);
            observatories = cell(1, n);
            for i = 1:n
                observatories{i} = xml.ObservatoryDescription{i}.Name.Text;
            end
        end
        
        % get datasets as a parsed xml structure
        function datasets = getDatasetsXml(obj, dataview, observatoryGroup, observatory, startDate, stopDate)
            request = [obj.url,'dataviews/',dataview,'/datasets'];
            if ~isnan(observatoryGroup) | ~isnan(observatory) | ~isnan(startDate) | ~isnan(stopDate) %#ok<OR2>
                request = [request,'?'];
            end
            if ~isnan(observatoryGroup)
                request = [request,'observatoryGroup=',observatoryGroup,'&'];
            end
            if ~isnan(observatory)
                request = [request,'observatory=',observatory,'&'];
            end
            if ~isnan(startDate)
                if ischar(startDate)
                    startDate = obj.datestr2isodate(startDate);
                else
                    startDate = obj.datenum2isodate(startDate);
                end
                request = [request,'startDate=',startDate,'&'];
            end
            if ~isnan(stopDate)
                if ischar(stopDate)
                    stopDate = obj.datestr2isodate(stopDate);
                else
                    stopDate = obj.datenum2isodate(stopDate);
                end
                request = [request,'stopDate=',stopDate,'&'];
            end
            datasets = obj.process(request).Datasets;
        end
        
        % get datasets as an array
        function datasets = getDatasets(obj, dataview, observatoryGroup, observatory, startDate, stopDate)
            xml = obj.getDatasetsXml(dataview, observatoryGroup, observatory, startDate, stopDate);
            n = length(xml.DatasetDescription);
            datasets= cell(1, n);
            for i = 1:n
                datasets{i} = xml.DatasetDescription{i}.Id.Text;
            end
        end
        
        % get variables as a parsed xml structure
        function variables = getVariablesXml(obj, dataview, dataset)
            request = [obj.url,'dataviews/',dataview,'/datasets/',dataset,'/variables'];
            variables = obj.process(request).Variables;
        end
        
        % get variables as an array
        function variables = getVariables(obj, dataview, dataset)
            xml = obj.getVariablesXml(dataview, dataset);
            n = length(xml.VariableDescription);
            variables= cell(1, n);
            for i = 1:n
                variables{i} = xml.VariableDescription{i}.Name.Text;
            end
        end
        
        % get data
        function data = getData(obj, dataview, dataset, startTime, stopTime, varargin)
            if ischar(startTime) & ischar(stopTime) %#ok<AND2>
                startTime = obj.datestr2isodate(startTime);
                stopTime = obj.datestr2isodate(stopTime);
            else
                startTime = obj.datenum2isodate(startTime);
                stopTime = obj.datenum2isodate(stopTime);
            end
            request = [obj.url,'dataviews/',dataview,'/datasets/',dataset,'/data/',startTime,',',stopTime,'/',strjoin(varargin,','),'?format=text'];
            xml = urlread(request);
            fid = fopen(obj.xmlFile, 'w');
            fprintf(fid, xml);
            fclose(fid);
            
            xml = xml2struct(obj.xmlFile);
            dataUrl = xml.DataResult.FileDescription.Name.Text;
            delete(obj.xmlFile);
            
            urlwrite(dataUrl, obj.dataFile);

            fid = fopen(obj.dataFile);
            fline = '#';
            while ischar(fline) && strncmp(fline, '#', 1)
                fline = fgetl(fid);
            end
            
            format = '%23c';
            for i = 1:length(strsplit(fline, ' ', 'CollapseDelimiters', 1))-1
                format = [format, ' %f']; %#ok<AGROW>
            end
            
            data = textscan(fid, format, 'CommentStyle', '#', 'MultipleDelimsAsOne', 1, 'HeaderLines', 2);
            fclose(fid);
            
            delete(obj.dataFile);
            
            data{1} = datenum(data{1}, 'dd-mm-yyyy HH:MM:SS.FFF');
        end
        
        % REST and xml2struct conversion
        function xml = process(obj, request)
            xml = urlread(request);
            fid = fopen(obj.xmlFile, 'w');
            fprintf(fid, xml);
            fclose(fid);
            xml = xml2struct(obj.xmlFile);
            delete(obj.xmlFile);
        end
        
        function isodate = datestr2isodate(obj, date)
            isodate = obj.datenum2isodate(datenum(date, 'yyyy-mm-dd HH:MM:SS'));
        end
        
        function isodate = datenum2isodate(obj, date) %#ok<INUSL>
            isodate = datestr(date, 'yyyymmddTHHMMSSZ');
        end
    end
    
end
