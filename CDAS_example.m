%#ok<*NOPTS>
'clearing all the exsiting variables from the workspace'
clear;
'initializing an object of CDAS class'
REST = CDAS;
'list of dataviews in xml format'
dataviews = REST.getDataviewsXml()
'1st dataview'
dataviews.DataviewDescription{1}.Id
'list of dataviews in array format'
REST.getDataviews()
'list of observatory groups in xml format for dataview=istp_public'
observatoryGroups = REST.getObservatoryGroupsXml('istp_public')
'1st observatory group'
observatoryGroups.ObservatoryGroupDescription{1}.Name.Text
'list of observatory groups in array format for dataview=istp_public'
REST.getObservatoryGroups('istp_public')
'list of observatories in xml format for dataview=istp_public'
observatories = REST.getObservatoriesXml('istp_public')
'1st observatory'
observatories.ObservatoryDescription{1}.Name
'list of observatories in array format'
REST.getObservatories('istp_public')
'list of datasets for GOES_12 observatory in xml format'
datasets = REST.getDatasetsXml('istp_public', 'GOES', 'GOES_12', '2005-09-23 00:00:00', '2005-09-24 00:00:00')
'1st dataset'
datasets.DatasetDescription{1}.Id
'list of datasets for GOES_12 observatory in array'
REST.getDatasets('istp_public', 'GOES', 'GOES_12', '2005-09-23 00:00:00', '2005-09-24 00:00:00')
'list of variables in GOES dataset G0_K0_MAG in xml format'
variables = REST.getVariablesXml('istp_public', 'G0_K0_MAG')
'1st variable'
variables.VariableDescription{1}.Name
'list of variables in GOES dataset G0_K0_MAG in array format'
REST.getVariables('istp_public', 'G0_K0_MAG')
'data for GOES_12 observatory for variable B_GSE_c'
data = REST.getData('istp_public', 'G0_K0_MAG', '2005-09-23 00:00:00', '2005-09-24 00:00:00', 'B_GSE_c', 'B_GSM_c')